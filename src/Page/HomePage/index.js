import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Landing from '../../containers/Landing';
import HistorySection from '../../containers/HistorySection';
import AppSection from '../../containers/AppSection';
import CategoriesSection from '../../containers/CategoriesSection';
import PopularSection  from '../../containers/PopularSection';
import DiscountSection from '../../containers/DiscountSection'

const HomePage = (props) => {

  return (
    <React.Fragment>
      <Landing />
      <HistorySection />
      <AppSection />
      <CategoriesSection />
      <DiscountSection />
      <PopularSection />
     
    </React.Fragment>
  )

}


export default HomePage;
