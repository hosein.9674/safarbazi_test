import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.scss';

const categoryItem = (props) => {
const {src,href,alt}=props;
  return (
    <React.Fragment>
       <li>
 <a href={href}>
 <div className="logo-1">
 <img src={src} alt={alt} />
 </div> 
 </a>
 </li> 
    </React.Fragment>
  )

}


export default categoryItem;
