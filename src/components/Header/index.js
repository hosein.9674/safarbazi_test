import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.scss'



const DefaultHeader = (props) => {

 
    return (
      <React.Fragment>
        <div className="search-section__header__wrapper">
          <header className="search-section__header">
            <nav className="search-section__header__menu">
              <h2 className="semantic_hide">منو کاربری</h2>
              <ul>
                <li className="search-section__header__user">
                  {/* <div className="search-section__header__user__icon"></div> */}
                  <div className="search-section__header__user__name">
                    <div className="search-section__header__login main-link header__login-link" id="modal-launcher"
                      data-toggle="modal" data-target="#login">ورود/عضویت</div>
                  </div>
                </li>
              </ul>
              <div className="language-sign">
                <a href="/">En</a>
              </div>
              <div>
              </div>
            </nav>
            <a href="/">
              <img src={require('../../assets/img/logo.svg')} alt="لوگو اسنپ فود" className="search-section__header__logo" />
              <h1 className="semantic_hide">سفارش آنلاین غذا در اسنپ‌فود</h1>
            </a>
          </header>
        </div>

      </React.Fragment>
    )
}
    export default DefaultHeader;