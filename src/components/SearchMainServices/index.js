import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchMainService from '../MainSectionService'
import './style.scss';

const SearchMainServices = (props) => {


    return (
        <React.Fragment>

            <div className="search-section__main__services">
                <SearchMainService className="service--restaurant" href="#" src={require('../../assets/img/services/restaurant.png')}
                    alt="رستوران "  number="۳۱۴" title="رستوران" />

                <SearchMainService className="service--cafe" href="#" src={require('../../assets/img/services/cafe.png')}
                    alt="کافی شاپ"  number="۳۱۴" title="کافی شاپ" />

                <SearchMainService className="service--confectionery" href="#" src={require('../../assets/img/services/confectionery.png')}
                    alt="شیرینی " number="۳۱۴" title="شیرینی" />
                <SearchMainService className="service--supermarket" href="#"
                    src={require('../../assets/img/services/supermarket.png')}
                    alt="سوپرمارکت "  number="۳۱۴" title="سوپرمارکت" />


            </div>


        </React.Fragment >
    )
}

export default SearchMainServices;
