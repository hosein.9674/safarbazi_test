import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.scss';

const History = (props) => {


    return (
        <React.Fragment>


            <section className="history-section-wrapper">
                <div className="history-section">
                    <header>
                        <h1 className="homepage__titr">
                            <span className="pink-text">اسنپ‌فود</span> پیشگام در سفارش آنلاین غذا در ایران
            </h1>
            </header>
            <p className="homepage__description">
                    اسنپ‌فود یا همان زودفود سابق، اولین و بزرگ‌ترین سامانه سفارش آنلاین غذا است. اسنپ‌فود، علاوه بر سفارش غذا، خدمات سفارش آنلاین کیک و شیرینی و سوپرمارکت آنلاین را نیز ارائه می‌کند.
                    در حال حاضر، بیش از ۱۳۵۰۰ رستوران دربیش از ۷۰ نقطه از کشور فعال هستند. شما با سفارش غذا از اسنپ فود به طیف وسیعی از رستوران‌ها، غذاها و بهترین برندها دسترسی خواهید داشت.
            با <a href="/restaurant/city/tehran" target="_blank">سفارش آنلاین غذا در تهران</a>، اصفهان، مشهد، تبریز و دیگر نقاط ایران، راحتی و تنوع را تجربه کنید.
        </p></div></section>

        </React.Fragment >
    )
}

export default History;
