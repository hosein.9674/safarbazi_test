import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';

import './style.scss';

const SearchMainService = (props) => {

    const {className,href,src,alt,number,title}=props
    return (
        <React.Fragment>

<a className={className} href={href}>
                    <figure className="search-section__main__services__item">
                        <img src={src} alt={alt}/>
                        <div className="search-section__main__services__number"><p>
                            {number}
                                                    </p></div>
                            <figcaption className="search-section__main__services__title"><p>
                            {title}                         </p></figcaption></figure></a>
        </React.Fragment >
    )
}

export default SearchMainService;
