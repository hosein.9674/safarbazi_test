
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.scss'

const DiscountItem = (props) => {
    
const{percent,src,name,area}=props;

    return (
        <React.Fragment>
        
                        <a href="/" className="daily-discount-section__logo">
                             <div className="daily-discount-section__discount">
                                 <p>{percent}</p>
                             </div>
                                 <figure>
                                     <img alt="alt" className="daily-discount-section__img"
                                      src={src}/>
                                      <figcaption>
                                         <p className="daily-discount-section__name">{name}</p>
                                         <p className="daily-discount-section__area">{area}</p>
                                      </figcaption>
                                </figure>
                        </a>
                
        </React.Fragment>
    )
}
export default DiscountItem;

