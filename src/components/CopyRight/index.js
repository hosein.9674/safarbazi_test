
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.scss';

const FooterLists = (props) => {


    return (
        <React.Fragment>
        <div class="copyright"><p>
        ©2019 <b><span class="pink-text copyright-dot">.</span> Snappfood <span class="pink-text copyright-dot">.</span></b> All rights reserved
    </p></div>

        </React.Fragment>
    )
}
export default FooterLists;

