import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Input from '../Input'
import './style.scss';

const SearchBox = (props) => {


    return (
        <React.Fragment>
            
            <div className="area-search-wrapper area-active-search-box">
    <div className="area-search">
                       <Input placeholder="شهر" className=" area-search-section--city"/>
                       <Input placeholder="محله" className=" area-search-section--area"/>
                       <input placeholder="غذا" className="homepage__input area-search-section--food" />
                       
                       <Input placeholder="رستوران" className=" area-search-section--restaurant"/>
                       
                        <div className="homepage__btn area-search-section--search"><p>جستجو</p></div>                         
                        </div></div>


        </React.Fragment >
    )
}

export default SearchBox;
