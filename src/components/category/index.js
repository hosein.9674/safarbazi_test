import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import './style.scss';

const Category = (props) => {

const{data_title,href,src,alt}=props
    return (
        <React.Fragment>


            <a className="cuisine-item" data-title={data_title } href={href}>
                <div>
                    <img src={src} alt={alt}/>
                </div>
            </a>

        </React.Fragment >
    )
}

export default Category;
