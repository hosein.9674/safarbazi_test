import React, { Component } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import HomePage from '../../Page/HomePage'
import DefaultHeader from '../../components/Header'
import FooterSection from '../FooterSection'

class DefaultLayout extends Component {
 
  



  render() {
   
    
    return (
      <div className="app">
        
            <DefaultHeader />
          
         
        <div className="app-body">
         
           
           
           
          <main >
           
            <article>
             
                <HomePage />
         
            </article>
          </main>
          <FooterSection />
        </div>
        
      </div>
    );
  }
}

export default DefaultLayout;
