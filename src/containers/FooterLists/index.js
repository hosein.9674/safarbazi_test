
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';


const FooterLists = (props) => {


    return (
        <React.Fragment>
            <footer className="newfooter__lists">
                <div className="know-more">
                    <div className="footer-list-title first-col">
                        <p>بیشتر بدانید  </p>
                    </div>
                    <ul>
                        <li><a href="/">درباره ما</a></li>
                        <li><a href="/">قوانین سایت</a></li>
                        <li><a href="/">پرسش‌های متداول</a></li>
                        <li><a href="/" >وبلاگ</a></li>
                        <li><a href="/" >سفارش شبانه غذا</a></li>
                        <li><a href="/">بسته‌های خدماتی اسنپ‌فود</a></li>
                        <li><a href="/">راهنمای پرداخت جیرینگ </a></li>
                        <li><a href="/">اپلیکیشن موبایل</a></li>
                        <li><a href="/">همکاری مشترک با سازمان ملل</a></li>
                        <li><a href="/" >مرام نامه همکاری ویژه</a></li>
                    </ul>
                </div>
                <div className="contact-us">
                    <div className="footer-list-title">
                        <p>با ما در ارتباط باشید </p>
                    </div>
                    <ul>
                        <li><a href="/">تماس با ما</a></li>
                        <li><a href="/">تیم ما</a></li>
                        <li><a href="/">همکاری با ما</a></li>
                        <li><a href="/">حریم خصوصی</a></li>
                        <li><a href="/">ثبت شکایت</a></li>
                    </ul>
                </div>
                <div className="enamad" >
                    <div className="footer-list-title">
                        <p>مجوزها </p>
                    </div>
                    <div className="enamad-pics" >
                        {/* <img className="enamad-img" id="xzjJOBtSHm9NSiFaYy2l"  
                        src="https://trustseal.enamad.ir/logo.aspx?id=65397&Code=xzjJOBtSHm9NSiFaYy2l" alt=""/>
                            <br /> */}
                        <img 
                        src="https://static.snapp-food.com/bundles/bodofoodfrontend/images/kasbokar.png" alt="کسب و کار مجازی" />
                    </div>
                </div>
                <div className="subscription">
                    <div className="footer-list-title">
                        <p>عضویت در خبرنامه</p>
                    </div>
                    <div className="homepage__description">
                        <p>برای عضویت در خبرنامه اسنپ‌فود و دریافت جدیدترین مطالب تخفیف‌‌ها و جشنواره‌ها ایمیل خود را وارد کنید</p>
                    </div>
                    <div className="homepage__couple-input-btn">
                        <input className="homepage__input" placeholder="ایمیل" />
                            <div className="homepage__btn">
                            <p>عضویت</p>
                            </div>
                </div>
                    <div className="newfooter__line">
                          <div className="newfooter__line-first"></div>
                          <div className="newfooter__line-second"></div>
                    </div>
                          <ul className="social-list">
                              <li><a className="aparat" href="/"> </a></li>
                              <li><a className="twitter" href="/"  > </a></li>
                              <li><a className="linkedin" href="/" > </a></li>
                              <li><a className="instagram" href="/" > </a></li>
                              <li><a className="telegram" href="/"  > </a></li>
                         </ul>
                   </div>
                </footer>

        </React.Fragment>
    )
}
export default FooterLists;

