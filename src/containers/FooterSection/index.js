
import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import FooterCities from '../../components/FooterCities';
import FooterLists from '../../containers/FooterLists'


const FooterSection = (props) => {


    return (
        <React.Fragment>

            <div className="newfooter">
                
                <FooterCities />
                <div className="newfooter__text">
                    <h2 className="newfooter__cities-title footer-list-title">از اسنپ فود بیشتر بدانیم!</h2>
                    <p className="homepage__description">اسنپ فود، اولین و بزرگ‌ترین سامانه آنلاین سفارش و دلیوری غذا است که سفارش اینترنتی غذا را برای همه امکان‌پذیر کرده است.
                زودفود، اولین برند سفارش آنلاین غذا در ایران است که با هدف پایه گذاری فرهنگ سفارش اینترنتی غذا کار خود را آغاز کرد. در سال ۱۳۹۶، شرکت های اسنپ و زودفود تحت نام تجاری مشترک اسنپ فود به فعالیت خود ادامه دادند تا تحولی جدید در حوزه سفارش غذا و فست فود ایجاد کنند.
                هم اکنون اسنپ‌فود به عنوان اولین سایت سفارش غذا در فضای کسب و کارهای آنلاین ایران، با بیش از ۱۳۵۰۰ رستوران از معتبرترین و بهترین رستوران‌های ایران مشغول همکاری است.
                هوس غذای ایرانی یا غذای ایتالیایی کرده‌اید؟ سفارش پیتزا یا سفارش شیک و گلاسه را در نظر گرفته‌اید؟ می‌خواهید برای سفارش صبحانه از خدمات دلیوری و ارسال قهوه داغ در محل کارتان لذت ببرید؟ یا شاید حتی مهمان دارید و می‌خواهید خدمات سفارش شبانه غذا دریافت کنید؟
                اسنپ فود همواره کنار شما است. سایت اسنپ فود را باز کنید یا با دانلود اپلیکیشن آن، با خیالی آسوده سفارش دهید.</p>
                </div>
                <div className="newfooter__line">
                    <div className="newfooter__line-first">
                        </div><div className="newfooter__line-second"></div></div>
               <FooterLists />
               </div>
                </React.Fragment>
                )
                }
 export default FooterSection;

              