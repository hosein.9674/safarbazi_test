import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import Category from '../../components/category'


const CategoriesSection = (props) => {


    return (
        <React.Fragment>
            <section className="category-section-wrapper">
                <div className="category-section">
                <Category alt="ساندویچ" href="#" data_title={9} src={require('../../assets/img/categories/1.png')}/>
                <Category alt="ایرانی" href="#" data_title={9}  src={require('../../assets/img/categories/2.png')}/>
                <Category alt="سالاد" href="#" data_title={9}  src={require('../../assets/img/categories/3.png')}/>
                <Category alt="دریایی" href="#" data_title={9}  src={require('../../assets/img/categories/16.png')}/>
                <Category alt="برگر" href="#" data_title={9}  src={require('../../assets/img/categories/13.png')}/>
                <Category alt="سوخاری" href="#" data_title={9}  src={require('../../assets/img/categories/10.png')}/>
                <Category alt="استیک" href="#" data_title={9} src={require('../../assets/img/categories/15.png')}/>
                <Category alt="لبنانی" href="#" data_title={9}  src={require('../../assets/img/categories/12.png')}/>
               
                </div>
            </section>
        </React.Fragment >
    )
}

export default CategoriesSection;


