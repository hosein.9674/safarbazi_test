import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import CategoryItem from '../../components/CategoryItem'


const PopularSection = (props) => {


    return (
        <React.Fragment>

            <div className="popular-section-wrapper">
                <section className="popular-section">
                    <div className="popular-section__text">
                        <header>
                            <h1 className="homepage__titr">
                            پرطرفدارهای <span className="pink-text">اسنپ‌فود&nbsp;</span>
                           </h1>
                        </header>
                <p className="homepage__description">مجموعه‌ای از بهترین و پرطرفدارترین رستوران‌ها و فروشگاه‌های فعال در اسنپ‌فود </p>
                    <ul className="popular-section__logos">
                       <CategoryItem alt="" href="#" src={require('../../assets/img/popularItems/nansahar.png')}/>
                       <CategoryItem alt="" href="#" src={require('../../assets/img/popularItems/baguette.png')}/>
                       <CategoryItem alt="" href="#" src={require('../../assets/img/popularItems/bono.png')}/>
                       <CategoryItem alt="" href="#" src={require('../../assets/img/popularItems/doctor.png')}/>
                       <CategoryItem alt="" href="#" src={require('../../assets/img/popularItems/hiva.png')}/>
                       <CategoryItem  alt="" href="#" src={require('../../assets/img/popularItems/mansoor.png')}/>
                       <CategoryItem  alt="" href="#" src={require('../../assets/img/popularItems/nader.png')}/>
                       <CategoryItem  alt="" href="#" src={require('../../assets/img/popularItems/reyhoon.png')}/>
                       <CategoryItem  alt="" href="#" src={require('../../assets/img/popularItems/shemroon.png')}/>
                       <CategoryItem  alt="" href="#" src={require('../../assets/img/popularItems/tarighat.png')}/>
                     
                    </ul>
                            
                         </div>              
                         <div className="popular-section__dish">
                             <img  alt="spoon" className="spoon" src={require('../../assets/img/spoon.png')}/>
                             <img alt="dish" className="dish" src={require('../../assets/img/food2.png')}/>
                             <img  alt="fork" className="fork" src={require('../../assets/img/fork.png')} />
                             </div>
                            </section>
                            </div>
        </React.Fragment >
    )
}

export default PopularSection;


 