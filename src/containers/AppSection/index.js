import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';



const AppSection = (props) => {


    return (
        <React.Fragment>

            <div className="app-section-wrapper">
                <section className="app-section">
                <div className="app-section__text">
                    <header><h1 className="homepage__titr"> اپلیکیشن <span className="pink-text">اسنپ‌فود&nbsp;</span></h1></header>
                    <p className="homepage__description">
                    با اپلیکیشن اسنپ‌فود به راحتی و با چند کلیک ساده می‌توانید رستوران‌ها، کافه‌ها، شیرینی‌فروشی‌ها
                     و سوپرمارکت‌های نزدیک خودتان را جست‌و‌جو کرده و از تجربه سفارش آسان از اسنپ‌فود لذت ببرید.             </p>
                     <p className="homepage__input-description"> برای دریافت لینک دانلود اپلیکیشن، شماره موبایل خود را وارد کنید.</p>
                    <div className="homepage__couple-input-btn">
                        <input className="homepage__input" maxLength="11" placeholder="*********۰۹" />
                        <div className="homepage__btn">
                            <p>دریافت لینک  </p>
                        </div>
                    </div>
                    <div className="app-section__links">
                        <a href="/">
                            <div className="googleplay"></div>
                        </a>
                        <a href="/">
                            <div className="cafebazaar"></div>
                        </a>
                                
                        <a href="/">
                            <div className="ios"></div>
                        </a>
                        <a href="/">
                          <div className="sibapp"></div>
                        </a>
                        <a href="/">
                            <div className="android"></div>
                        </a>
                        <a  href="/" className="fill-empty">
                            <div className="fill-empty"></div>
                        </a>
                    </div>
                 
                </div>
                <div className="app-section__image">
                    <img alt="mobileImage" className="mobileImage" src={require('../../assets/img/iphoneX_mockup.png')} />        
                    </div>
                    </section>
                    </div>
          
        </React.Fragment >
    )
}

export default AppSection;
