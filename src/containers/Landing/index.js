import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import SearchBox from '../../components/SearchBox';
import SearchMainServices from '../../components/SearchMainServices';


const Landing = (props) => {


    return (
        <React.Fragment>


            <div className="search-section">
                <div className="search-section__top-layer"></div>
                <div className="search-section__bottom-layer"></div>
                <header className="semantic_hide"><h1>سفارش آنلاین غذا از رستوران های اطراف شما</h1></header>
                <section className="search-section__main">
                    <header><h1 className="Page_title">سفارش آنلاین غذا از بهترین رستوران‌های اطراف شما</h1>
                    </header>
                 
                    <SearchBox />
                   <SearchMainServices />
                </section>
            </div>

        </React.Fragment >
    )
}

export default Landing;
