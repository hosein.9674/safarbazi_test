
import React, { useState } from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import DiscountItem from '../../components/DiscountItem'

const items = [
    {
        name: 'تهران',
        index: '1',

    },
    {
        name: 'اصفهان',
        index: '2',

    },
    {
        name: 'مشهد',
        index: '3',

    },
    {
        name: 'شیراز',
        index: '4',

    },
    {
        name: 'تبریز',
        index: '5',

    },
    {
        name: 'گیلان',
        index: '6',

    },
    {
        name: 'اردبیل',
        index: '7',

    },
    {
        name: 'شهرکرد',
        index: '8',

    },
    {
        name: 'شهرری',
        index: '9',

    },
    {
        name: 'نجف آباد',
        index: '10',

    },
    {
        name: 'قم',
        index: '11',

    },
    {
        name: 'کاشان',
        index: '12',

    },

];

const DiscountSection = (props) => {

const[rightOffset,setRightOffset]=useState(0);

const[activeTab,setActiveTab]=useState('1');

const prev=()=>{
    if(rightOffset===0)return;
    let des=rightOffset+113
    setRightOffset(des)
    
}
const next=()=>{
   
    if(rightOffset===-((items.length -7)*113))return;
    let des=rightOffset-113;
    setRightOffset(des)
   
}
const toggle = (tab)=> {
    if (activeTab !== tab) {
        setActiveTab(tab)
    }

}
    return (
        <React.Fragment>
            <section className="daily-discount-section">
                <header><h1 className="homepage__titr">   تخفیف‌های روزانه   <span className="pink-text">اسنپ‌فود</span>&nbsp;</h1></header>
                <p className="homepage__description"></p>
                <div className="daily-discount-section__logo-container">
                    <div className="daily-discount-section__cities">
                        <div className="arrow-right" onClick={prev}>
                             <i className="right" ></i>
                        </div> 
                        <div className="daily-discount-section__cities__list-container">
                       


                           
                           
                           
                            <ul className="daily-discount-section__cities__list" style={{right:`${rightOffset}px`}} >
                           
                                {items.map(item => (
                                  
                                        <li key={item.index} className={`daily-discount-section__cities__list__item ${(activeTab === item.index)&& 'active-city'}`} 
                                        id={item.index}
                                        onClick={()=>toggle(item.index)}
                                        >
                                            <span>{item.name}</span>
                                            </li>)
                                )}
                             </ul> 
                        </div>
                        <div className="arrow-left"  onClick={next}>
                             <i className="left"></i>
                        </div> 
                    </div>
                    <div className="daily-discount-section__logos">
                        <DiscountItem area="کشاورز" name="P.S (فیلیپر)" percent="۱۵%"
                            src={require('../../assets/img/discountSection/1.jpg')}
                        />
                        <DiscountItem area="یوسف آباد" name="لوازم بهداشتی و ضد عفونی کننده ارکیده" percent="۱۵%"
                            src={require('../../assets/img/discountSection/1.jpg')}
                        />
                        <DiscountItem area="کشاورز" name="P.S (فیلیپر)" percent="۱۵%"
                            src={require('../../assets/img/discountSection/1.jpg')}
                        />
                        <DiscountItem area="کشاورز" name="P.S (فیلیپر)" percent="۱۵%"
                            src={require('../../assets/img/discountSection/1.jpg')}
                        />
                        <DiscountItem area="کشاورز" name="P.S (فیلیپر)" percent="۱۵%"
                            src={require('../../assets/img/discountSection/1.jpg')}
                        />
                        <DiscountItem area="کشاورز" name="P.S (فیلیپر)" percent="۱۵%"
                            src={require('../../assets/img/discountSection/1.jpg')}
                        />
                        <DiscountItem area="کشاورز" name="P.S (فیلیپر)" percent="۱۵%"
                            src={require('../../assets/img/discountSection/1.jpg')}
                        />
                    </div>
                </div>
            </section>

        </React.Fragment>
    )
}
export default DiscountSection;

